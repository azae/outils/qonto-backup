import json

import requests


def dump(filename, dict_):
    with open(filename, 'w') as f:
        d = json.dumps(dict_, indent=4, sort_keys=True)
        print(d, file=f)


def download_file(url, filename):
    # NOTE the stream=True parameter below
    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        with open(filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)
                    # f.flush()
    return filename