import os

QONTO_ORG = os.environ['QONTO_ORG']
QONTO_PASS = os.environ['QONTO_PASS']
QONTO_HEADER = f"{QONTO_ORG}:{QONTO_PASS}"
DATA_DIR = os.environ.get('DATA_DIR', 'qonto')
TRANSACTIONS_DIR = f"{DATA_DIR}/transactions"
ATTACHMENTS_DIR = f"{DATA_DIR}/attachments"