import glob
import json
import os
import shutil
from pathlib import Path

from conf import QONTO_ORG, QONTO_HEADER, TRANSACTIONS_DIR, ATTACHMENTS_DIR
from lib import dump, download_file
from swagger import create_client


def get_orga():
    o = client.organizations.GET_organizations_id(**{"id": QONTO_ORG, 'Authorization': QONTO_HEADER}).response().result
    iban = o['organization']['slug']
    slug = o['organization']['bank_accounts'][0]['slug']
    return iban, slug


def get_transactions(iban, slug):
    return client.transactions.GET_transactions(
        **{'slug': slug, 'iban': iban, 'Authorization': QONTO_HEADER}).response().result


def dump_transactions(transactions):
    for transaction in transactions['transactions']:
        id = transaction['transaction_id']
        dump(f'{TRANSACTIONS_DIR}/{id}.json', transaction)


def get_attachment_metadata(id):
    m = client.attachments.GET_attachments_id(**{"id": id, 'Authorization': QONTO_HEADER}).response().result
    return m


def get_attachment_ids():
    for file_name in glob.glob(f'{TRANSACTIONS_DIR}/*.json'):
        with open(file_name) as f:
            data = json.load(f)
            attachment_ids = data['attachment_ids']
            for id in attachment_ids:
                yield {
                    "attachment_id": id,
                    "transaction_id": data["transaction_id"]
                }


def delete_uncoherent_attachments():
    for filename in glob.glob(f'{ATTACHMENTS_DIR}/*'):
        if len(list(Path(filename).glob('*'))) != 2:
            shutil.rmtree(filename)


def download_attachments():
    for attachment_ids in get_attachment_ids():
        attachment_id = attachment_ids["attachment_id"]
        attachment_dir = f'{ATTACHMENTS_DIR}/{attachment_id}'
        if os.path.exists(attachment_dir):
            continue

        Path(attachment_dir).mkdir(parents=True)
        metadata = get_attachment_metadata(attachment_id)
        dump(f"{attachment_dir}/meta.json", metadata)
        url = metadata['attachment']['url']
        download_file(url, f"{attachment_dir}/{metadata['attachment']['file_name']}")


def main():
    iban, slug = get_orga()
    transactions = get_transactions(iban, slug)
    dump_transactions(transactions)
    delete_uncoherent_attachments()
    download_attachments()


if __name__ == '__main__':
    client = create_client()
    main()
