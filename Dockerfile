FROM       python:3.7-alpine3.10
RUN        pip install pipenv && mkdir /data
COPY       . /app
WORKDIR    /app
RUN        pipenv install --deploy
ENV        DATA_DIR=/data
ENV        SHELL=/bin/bash
ENTRYPOINT ["pipenv", "run"]
CMD        ["python", "backup.py"]
