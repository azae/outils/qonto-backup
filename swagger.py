from bravado.client import SwaggerClient
from bravado.requests_client import RequestsClient
from bravado.swagger_model import load_file

from conf import QONTO_HEADER


def create_client():
    http_client = RequestsClient()
    http_client.set_api_key(
        'http://thirdparty.qonto.eu',
        QONTO_HEADER,
        param_name='Authorization',
        param_in='header',
    )
    swag = load_file('swag.json')
    client = SwaggerClient.from_spec(swag, http_client=http_client, config={

        # === bravado-core config ====

        #  validate incoming responses
        'validate_responses': True,

        # validate outgoing requests
        'validate_requests': True,

        # validate the swagger spec
        'validate_swagger_spec': False,

        # Use models (Python classes) instead of dicts for #/definitions/{models}
        # 'use_models': True,

    })
    return client