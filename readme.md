# Qonto backup

The aim of this project is to backup qonto transactions and their attachments.

## File structure

    attachments
    ├── 0348583b-b430-45bf-ba4d-0b8d4a32cbfb
    │   ├── 2020-03-26_10.36.02_ndf.pdf
    │   └── meta.json
    ├── 0348d83b-b43s-4lkj-bd4d-0b8d4a32cbfb
    │   ├── 2020-03-24_10.36.02_ndf.pdf
    │   └── meta.json
    └── ....
    transactions
    ├── myorg-1-transaction-1.json
    ├── myorg-1-transaction-2.json
    ├── myorg-1-transaction-3.json
    └── ....

## Known limitations

* More than 100 transactions is not supported yet but it will be.

## Contribute

Feel free to start discussion with an issue or a MR :)